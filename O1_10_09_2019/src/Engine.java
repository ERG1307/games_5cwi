
public class Engine {
	private int horsePower;
	private String type;
	
	public Engine(int horsePower, String type) {
		super();
		this.horsePower = horsePower;
		this.type = type;
	}
	

	public int getHorsePower() {
		return horsePower;
	}

	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
