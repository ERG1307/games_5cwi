
public class Car {
	private String color;
	private String model;
	private Engine engine;
	
	public Car(String color, Engine engine) {
		this.color = color;
		this.engine = engine;
	}
	
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}


	public Engine getEngine() {
		return engine;
	}


	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
	
}
