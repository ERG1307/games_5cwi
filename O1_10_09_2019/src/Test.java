import java.awt.Color;

public class Test 
{

	public static void main(String[] args) 
	{
		Engine e1 = new Engine(200,"diesel");
		Engine e2 = new Engine(300,"super");

		Car c1 = new Car("blue", e1);
		
		c1.getEngine().setHorsePower(331312310);
		Car c2 = new Car("red", e2);
		Car c3 = c2;
	
		System.out.println(e1.getHorsePower());

	}
	
}
