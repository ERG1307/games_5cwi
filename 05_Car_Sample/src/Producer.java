
public class Producer {
	private String name;
	private String country;
	private double rabatt;
	
	public Producer(String name, String country, double rabatt) {
		super();
		this.name = name;
		this.country = country;
		this.rabatt = rabatt;
	}





	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public double getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	
}
