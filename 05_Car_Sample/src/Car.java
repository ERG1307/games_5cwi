
public class Car {
	private String color;
	private int price;
	private int speed;
	private double usage;
	private Engine engine;
	private Producer producer;
	private double petrolUsage;
	
	public Car (String color, int speed, double usage, int price, Engine engine, Producer producer, double petrolUsage) {
		this.color = color;
		this.speed = speed;
		this.setPetrolUsage(petrolUsage);
		this.usage = usage;
		this.engine = engine;
		this.producer = producer;
		this.price = price;
	}
	
	public int getDiscountPrice() {
		int r = (int) (producer.getRabatt() * price / 100);
		int rabatt = price - r;
		return rabatt;
	}
	
	
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public double getUsage() {
		return usage;
	}

	public void setUsage(double usage) {
		this.usage = usage;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public double getPetrolUsage() {
		if (this.usage > 50.000) {
			double u = petrolUsage * 1.098;
			petrolUsage = u;
		}
		return petrolUsage;
	}

	public void setPetrolUsage(double petrolUsage) {
		this.petrolUsage = petrolUsage;
	}
	
	
	
	
	
	
	
	

	
	
}
