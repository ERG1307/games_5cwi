
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;


public class Person {
	private String firstname, lastname;
	private List<Car> cars;
	private LocalDate birthdate;

	

	public Person(String firstname, String lastname, LocalDate birthdate) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		
		this.cars = new ArrayList<>();
	}


	
	public int getAge() {
		//int year = this.birthdate.getYear();
		//year - LocalDate.now().getYear();
		LocalDate currentDate = LocalDate.now();
		return Period.between(birthdate, currentDate).getYears();
		
	}
	
	public int getValueCars() {
		int sum = 0;
		for (Car car : cars) {
			sum = sum + car.getDiscountPrice();
			//System.out.println(sum);
		}
		return sum;
	}
	
	
	public void addCar(Car c) {
		this.cars.add(c);
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	
	
	
}
