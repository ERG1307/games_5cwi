import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		
		Producer p1 = new Producer("Benz", "Deutschland", 25.00);
		Producer p2 = new Producer("Audi", "Austria", 15.00);
		Engine e1 = new Engine("diesel", 200);
		Engine e2 = new Engine("eeee", 140);
		
		Car c1 = new Car("red", 100 ,50.001, 1000, e1, p1, 0.76);
		Car c2 = new Car("yellow", 120 ,50.501, 2000, e2, p2, 0.66);
		
		Person pe1 = new Person("heinz","schneider", LocalDate.of(2000, 07, 13));
		Person pe2 = new Person("George","Orwell", LocalDate.of(2015, 12, 30));
		
		
		
		pe1.addCar(c1);
		pe1.addCar(c2);
		pe2.addCar(c1);
		
		
		System.out.println(c1.getPrice());
		System.out.println(c1.getPetrolUsage());
		System.out.println(pe1.getAge());
		System.out.println(pe1.getValueCars());
		System.out.println(c1.getDiscountPrice());
		System.out.println(c2.getDiscountPrice());
	}
	
}
