
public class Battery {
	
	private int ChargingStatus;

	public Battery(int chargingStatus) {
		ChargingStatus = chargingStatus;
	}

	
	//getter & setter
	public int getChargingStatus() {
		return ChargingStatus;
	}

	public void setChargingStatus(int chargingStatus) {
		ChargingStatus = chargingStatus;
	}

	
}
	
	
