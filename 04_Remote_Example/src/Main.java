
public class Main {

	public static void main(String[] args) {
				
		Battery b1 = new Battery(100);
		Remote r1 = new Remote(true, true, b1);
		
		System.out.println(r1.isOn());
		System.out.println(r1.getStatus());
		
		r1.TurnOff();
		System.out.println(r1.isOn());
	}
}
