
public class Remote {
	
	private boolean isOn;
	private boolean hasPower;
	private Battery battery;
	
	
	
	public Remote(boolean isOn, boolean hasPower, Battery battery) {
		this.isOn = isOn;
		this.hasPower = hasPower;
		this.battery = battery;
	}
	
	// my methods
	public boolean TurnOn() {
		return isOn = true;
	}
	public boolean TurnOff() {
		return isOn = false;
	}
	public int getStatus() {
		int status;
		status = (battery.getChargingStatus());
		return status;
	}
	
	
	
	public Battery getBattery() {
		return battery;
	}

	public void setBattery(Battery battery) {
		this.battery = battery;
	}

	//getter & setter
	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public boolean getHasPower() {
		return hasPower;
	}

	public void setHasPower(boolean hasPower) {
		this.hasPower = hasPower;
	}
	
	
}
