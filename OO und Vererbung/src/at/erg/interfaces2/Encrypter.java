package at.erg.interfaces2;

public interface Encrypter {
	
	public String encrypt(String data);
	public String getFounder();
	
}
