package at.erg.interfaces2;

public abstract class AbstractEncrypter implements Encrypter{
	private String founder;

	public AbstractEncrypter(String founder) {
		super();
		this.founder = founder;
	}
	
	public String getFounder(){
		return founder;
	}
}
