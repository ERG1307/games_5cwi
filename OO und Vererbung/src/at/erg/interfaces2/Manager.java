package at.erg.interfaces2;

public class Manager {
	
	private Encrypter encryptor;


	public String  doEncrypt(String data) {
		return this.encryptor.encrypt(data);
	}

	public void setEncrypter(Encrypter e) {
		this.encryptor = e;
	}
}
