package at.erg.interfaces2;

public class Main {

	public static void main(String[] args) {
		
		Manager m1 = new Manager();	
		Encrypter e1 = new RC2("Albert Einstein");
		m1.setEncrypter(e1);
		String encryptedValue = m1.doEncrypt("1234");
		System.out.println(encryptedValue);
		System.out.println(e1.getFounder());
	}
}
