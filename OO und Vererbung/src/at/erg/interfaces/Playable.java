package at.erg.interfaces;

public interface Playable {
	
	public void play();
}
