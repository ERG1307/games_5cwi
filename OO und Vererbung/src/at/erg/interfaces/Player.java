package at.erg.interfaces;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private List<Playable> playables;

	public Player() {
		super();
		this.playables = new ArrayList<Playable>();
	}
	
	public void addPlayable(Playable pa) {
		this.playables.add(pa);
	}

	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}
	
	
}
	
	


