package at.erg.interfaces;



public class Main {

	public static void main(String[] args) {
		
		Title t1 = new Title();
		Item i1 = new Item();
		Song s1 = new Song();
		
		Player player = new Player();
		player.addPlayable(t1);
		player.addPlayable(i1);
		player.addPlayable(s1);
		
		player.playAll();
		
	}
}
