package at.ran.games.wintergame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {

	private CircleActor ca1;
	private OvalActor oa1;
	private RectActor ra1;
	
	public MainGame(String title) {
		super(title);
	
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//es wird gezeichnet
		this.ca1.render(graphics);
		this.oa1.render(graphics);
		this.ra1.render(graphics);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		//1 mal ausgeführt
		this.ca1 = new CircleActor(100, 100);
		this.oa1 = new OvalActor(420,100);
		this.ra1 = new RectActor(100,100);
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//wird laufende aufgerufen delta = zeit seit dem letzem aufruf
		this.ca1.update(gc, delta);
		this.oa1.update(gc, delta);
		this.ra1.update(gc, delta);
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
