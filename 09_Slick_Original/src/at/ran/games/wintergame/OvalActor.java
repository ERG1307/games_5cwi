package at.ran.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor {
	private int x;
	private int y;
	private boolean movingRight = true;
	
	public OvalActor(int x, int y) {
		super();
		this.x = x;
		this.y = x;
	}

	public void update (GameContainer gc, int delta) {

		
		if(movingRight)
		{
		   x+=1;
		}
		else
		{
		   x-=1;
		}

		if (x == 650) // turn around and go the other way
		{
		    movingRight = false;
		    
		}
		else if (x == 100) // turn around and go the other way
		{
		    movingRight = true;
		}

	}
	
	public void render (Graphics graphics) {
		    graphics.drawOval(this.x,this.y, 80, 20);
	}


	
}
