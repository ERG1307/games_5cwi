package at.ran.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor {

	private int x;
	private int y;
	private boolean movingRight = true;
	private boolean movingLeft;
	private boolean movingDown;
	private boolean movingUp;


	public RectActor(int x, int y) {
		super();
		this.x = x;
		this.y = y;

	}
	
	public void update (GameContainer gc, int delta) {
		
		// Nach rechts
		if(movingRight == true)
		{
		   x+=1;
		}
		if (x == 650) 
		{
		    movingRight = false;
		}
		
		//if ist nur dazu da damit man movingDown 1x auf true setzt.
		if (x == 650 && y == 100) {
			movingDown = true;
		}

		// Nach unten
		if (movingDown == true) 
		{
			y+=1;
		}
		if (y == 450) 
		{
			movingDown = false;
		}
		
		//if ist nur dazu da damit man movingLeft 1x auf true setzt.
		if (x == 650 && y == 450) {
			movingLeft = true;
		}
		
		// bewegt das rechteck nach links 
		if (movingLeft == true) 
		{
			x-=1;
		}
		if (x == 100) {
			movingLeft = false;
		}
		
		//if ist nur dazu da damit man movingUp 1x auf true setzt.
		if (x == 100 && y == 450) {
			movingUp = true;
		}
		
		// bewegt das rechteck nach links 
		if (movingUp == true) 
		{
			y-=1;
		}
		if (y == 100) {
			movingUp = false;
		}
		
		//if ist nur dazu da damit man movingRight 1x auf true setzt.
		if (x == 100 && y == 100) {
			movingRight = true;
		}
	}
	
	public void render (Graphics graphics) {
		  graphics.drawRect(this.x, this.y, 50, 50);
	}
	
		
	
}
